import nodePolyfills from 'rollup-plugin-polyfill-node'
import prettier from 'rollup-plugin-prettier'
import babel from '@rollup/plugin-babel'

export default {
  external: ['snake-case'],
  input: 'entry.js',
  output: [
    {
      file: 'index.js',
      format: 'esm',
    },
  ],
  plugins: [
    nodePolyfills({ include: ['path'] }),
    babel({ babelHelpers: 'bundled' }),
    prettier({ parser: 'babel' }),
  ],
}

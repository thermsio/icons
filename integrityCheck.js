import fs from 'fs'
import $ from 'cheerio'

let errors = 0

const testSvg = (svgPath) => {
  const viewBox = $.load(fs.readFileSync(svgPath))('svg').attr('viewBox')
  const [, , width, height] = viewBox.split(' ')

  // Ensure all icons have the same size.
  if (width !== height) {
    console.error(
      `Error: ${svgPath} has a viewBox that is not square`,
      (errors += 1),
    )
  }
}

const testAllFiles = (dir) => {
  fs.readdirSync(dir).forEach((file) => {
    const filePath = `${dir}/${file}`

    if (!fs.statSync(filePath).isDirectory()) {
      testSvg(filePath)
    }
  })
}

testAllFiles('src/icons')

if (errors > 0) {
  process.exit(1)
} else {
  console.log('Tests passed!')
}

# Module Specific References
## bulletin

bulletin_module

![bulletin_module](https://unpkg.com/@therms/icons@2//png/bulletin_module.png)

## dispatch

dispatch_module

![dispatch_module](https://unpkg.com/@therms/icons@2//png/dispatch_module.png)

## dashboard

dashboard_module

![dashboard_module](https://unpkg.com/@therms/icons@2//png/dashboard_module.png)

## events

events_module

![events_module](https://unpkg.com/@therms/icons@2//png/events_module.png)

## forms_layout

forms_layout_module

![forms_layout_module](https://unpkg.com/@therms/icons@2//png/forms_layout_module.png)

## geo

breadcrumb

![breadcrumb](https://unpkg.com/@therms/icons@2//png/pending.png)

enter

![enter](https://unpkg.com/@therms/icons@2//png/arrow_circle_right.png)

exit

![exit](https://unpkg.com/@therms/icons@2//png/arrow_circle_left.png)

geo_fence

![geo_fence](https://unpkg.com/@therms/icons@2//png/universal_local.png)

geo_tracks

![geo_tracks](https://unpkg.com/@therms/icons@2//png/track_changes.png)

gps

![gps](https://unpkg.com/@therms/icons@2//png/gps_fixed.png)

map

![map](https://unpkg.com/@therms/icons@2//png/map.png)

## location

access

![access](https://unpkg.com/@therms/icons@2//png/punch_clock.png)

bolo

![bolo](https://unpkg.com/@therms/icons@2//png/warning.png)

contact

![contact](https://unpkg.com/@therms/icons@2//png/contacts.png)

email_subscribers

![email_subscribers](https://unpkg.com/@therms/icons@2//png/rss_feed.png)

files

![files](https://unpkg.com/@therms/icons@2//png/collections.png)

location_module

![location_module](https://unpkg.com/@therms/icons@2//png/apartment.png)

log

![log](https://unpkg.com/@therms/icons@2//png/summarize.png)

main_location

![main_location](https://unpkg.com/@therms/icons@2//png/apartment.png)

pass_down

![pass_down](https://unpkg.com/@therms/icons@2//png/keyboard_double_arrow_down.png)

portal_users

![portal_users](https://unpkg.com/@therms/icons@2//png/account_circle.png)

post_orders

![post_orders](https://unpkg.com/@therms/icons@2//png/list_alt.png)

services

![services](https://unpkg.com/@therms/icons@2//png/summarize.png)

sub_location

![sub_location](https://unpkg.com/@therms/icons@2//png/business.png)

tasks

![tasks](https://unpkg.com/@therms/icons@2//png/checklist.png)

## mail

attachment

![attachment](https://unpkg.com/@therms/icons@2//png/attachment.png)

compose

![compose](https://unpkg.com/@therms/icons@2//png/edit.png)

forward

![forward](https://unpkg.com/@therms/icons@2//png/forward.png)

important

![important](https://unpkg.com/@therms/icons@2//png/priority_high.png)

inbox

![inbox](https://unpkg.com/@therms/icons@2//png/inbox.png)

outbox

![outbox](https://unpkg.com/@therms/icons@2//png/outbox.png)

read

![read](https://unpkg.com/@therms/icons@2//png/visibility.png)

reply_all

![reply_all](https://unpkg.com/@therms/icons@2//png/reply_all.png)

reply

![reply](https://unpkg.com/@therms/icons@2//png/reply.png)

send

![send](https://unpkg.com/@therms/icons@2//png/send.png)

unread

![unread](https://unpkg.com/@therms/icons@2//png/visibility_off.png)

## organization

organization_module

![organization_module](https://unpkg.com/@therms/icons@2//png/organization_module.png)

## patrol

patrol_module

![patrol_module](https://unpkg.com/@therms/icons@2//png/patrol_module.png)

route

![route](https://unpkg.com/@therms/icons@2//png/navigation.png)

route_active

![route_active](https://unpkg.com/@therms/icons@2//png/route.png)

## regions

regions_module

![regions_module](https://unpkg.com/@therms/icons@2//png/regions_module.png)

## record

item

![item](https://unpkg.com/@therms/icons@2//png/sell.png)

person

![person](https://unpkg.com/@therms/icons@2//png/person.png)

property

![property](https://unpkg.com/@therms/icons@2//png/sell.png)

record_module

![record_module](https://unpkg.com/@therms/icons@2//png/record_module.png)

vehicle

![vehicle](https://unpkg.com/@therms/icons@2//png/directions_car.png)

## report

inactive

![inactive](https://unpkg.com/@therms/icons@2//png/nights_stay.png)

pending

![pending](https://unpkg.com/@therms/icons@2//png/pending.png)

rejected

![rejected](https://unpkg.com/@therms/icons@2//png/dangerous.png)

reopened

![reopened](https://unpkg.com/@therms/icons@2//png/restart_alt.png)

report_module

![report_module](https://unpkg.com/@therms/icons@2//png/report_module.png)

## report_entry

arrest

![arrest](https://unpkg.com/@therms/icons@2//png/arrest.png)

basic_report

![basic_report](https://unpkg.com/@therms/icons@2//png/basic_report.png)

camera_activity

![camera_activity](https://unpkg.com/@therms/icons@2//png/camera_indoor.png)

report_entry

![report_entry](https://unpkg.com/@therms/icons@2//png/note_alt.png)

disciplinary_action

![disciplinary_action](https://unpkg.com/@therms/icons@2//png/assignment_late.png)

dispatch_call

![dispatch_call](https://unpkg.com/@therms/icons@2//png/phone_in_talk.png)

equipment_dls

![equipment_dls](https://unpkg.com/@therms/icons@2//png/home_repair_service.png)

foot_patrol

![foot_patrol](https://unpkg.com/@therms/icons@2//png/directions_walk.png)

interview

![interview](https://unpkg.com/@therms/icons@2//png/question_answer.png)

observation

![observation](https://unpkg.com/@therms/icons@2//png/supervisor_account.png)

on_duty_injury

![on_duty_injury](https://unpkg.com/@therms/icons@2//png/personal_injury.png)

peer_evaluation

![peer_evaluation](https://unpkg.com/@therms/icons@2//png/thumbs_up_down.png)

retail_theft

![retail_theft](https://unpkg.com/@therms/icons@2//png/production_quantity_limits.png)

statement

![statement](https://unpkg.com/@therms/icons@2//png/assignment_ind.png)

tag_scan

![tag_scan](https://unpkg.com/@therms/icons@2//png/qr_code_scanner.png)

task

![task](https://unpkg.com/@therms/icons@2//png/task.png)

use_of_force

![use_of_force](https://unpkg.com/@therms/icons@2//png/sports_mma.png)

vehicle_accident

![vehicle_accident](https://unpkg.com/@therms/icons@2//png/minor_crash.png)

vehicle_patrol

![vehicle_patrol](https://unpkg.com/@therms/icons@2//png/directions_car.png)

## report_type

camera_monitoring

![camera_monitoring](https://unpkg.com/@therms/icons@2//png/camera_indoor.png)

dar

![dar](https://unpkg.com/@therms/icons@2//png/assignment.png)

equipment_dls

![equipment_dls](https://unpkg.com/@therms/icons@2//png/home_repair_service.png)

firewatch

![firewatch](https://unpkg.com/@therms/icons@2//png/local_fire_department.png)

investigation

![investigation](https://unpkg.com/@therms/icons@2//png/search.png)

location

![location](https://unpkg.com/@therms/icons@2//png/apartment.png)

loss_prevention

![loss_prevention](https://unpkg.com/@therms/icons@2//png/store.png)

on_duty_injury

![on_duty_injury](https://unpkg.com/@therms/icons@2//png/personal_injury.png)

peer_evaluation

![peer_evaluation](https://unpkg.com/@therms/icons@2//png/thumbs_up_down.png)

report_type

![report_type](https://unpkg.com/@therms/icons@2//png/security.png)

special_event

![special_event](https://unpkg.com/@therms/icons@2//png/flag.png)

use_of_force

![use_of_force](https://unpkg.com/@therms/icons@2//png/sports_mma.png)

vehicle_accident

![vehicle_accident](https://unpkg.com/@therms/icons@2//png/minor_crash.png)

## roster

roster_module

![roster_module](https://unpkg.com/@therms/icons@2//png/users_module.png)

## schedule

schedule_module

![schedule_module](https://unpkg.com/@therms/icons@2//png/schedule_module.png)

## users

users_module

![users_module](https://unpkg.com/@therms/icons@2//png/users_module.png)

## wallboard

wallboard_module

![wallboard_module](https://unpkg.com/@therms/icons@2//png/web.png)


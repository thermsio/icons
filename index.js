import { snakeCase } from 'snake-case'

const BASE_URL = 'https://unpkg.com/@therms/icons@2/'

/**
 * Takes in a random cased name and returns the correct format for icon map.
 * @param iconName
 * @param featureName
 * @returns {{icon: (string), feature: (string)}|{}|{iconName: (string)}}
 */

const getFormattedName = function () {
  let iconName =
    arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : ''
  let featureName =
    arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ''
  const formattedNames = {
    icon: snakeCase(iconName),
    feature: featureName,
  }
  if (featureName) formattedNames.feature = snakeCase(featureName)
  return formattedNames
}
/**
 * Returns the URL where the icon is hosted.
 * @param name
 * @param extension
 * @returns {string|null}
 */

const getIconUrl = function (name) {
  let extension =
    arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'svg'
  if (!name) return null
  const formattedName = getFormattedName(name).icon
  return ''
    .concat(BASE_URL, '/')
    .concat(extension, '/')
    .concat(formattedName, '.')
    .concat(extension)
}

export { getFormattedName, getIconUrl }

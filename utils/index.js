import { snakeCase } from 'snake-case'
import { BASE_URL } from '../src/constants-web.js'

/**
 * Takes in a random cased name and returns the correct format for icon map.
 * @param iconName
 * @param featureName
 * @returns {{icon: (string), feature: (string)}|{}|{iconName: (string)}}
 */
export const getFormattedName = (iconName = '', featureName = '') => {
  const formattedNames = { icon: snakeCase(iconName), feature: featureName }
  if (featureName) formattedNames.feature = snakeCase(featureName)
  return formattedNames
}

/**
 * Returns the URL where the icon is hosted.
 * @param name
 * @param extension
 * @returns {string|null}
 */
export const getIconUrl = (name, extension = 'svg') => {
  if (!name) return null
  const formattedName = getFormattedName(name).icon

  return `${BASE_URL}/${extension}/${formattedName}.${extension}`
}

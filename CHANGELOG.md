# [2.19.0](http://bitbucket.org/thermsio/icons/compare/v2.18.0...v2.19.0) (2025-02-07)


### Bug Fixes

* **CORE-2819:** resolve merge conflict; ([b489765](http://bitbucket.org/thermsio/icons/commits/b489765c7a5f25174411e311909314fc5928517d))


### Features

* **CORE-2819:** add wallboard module icon; ([16c5037](http://bitbucket.org/thermsio/icons/commits/16c5037057e7f3489b3fa455aaebe158e0a6eaa9))

# [2.18.0](http://bitbucket.org/thermsio/icons/compare/v2.17.0...v2.18.0) (2024-04-01)


### Features

* add geo.geo_fence icon ([c104ab9](http://bitbucket.org/thermsio/icons/commits/c104ab93e3b2ecba159e488ab0dadd81a15b13b0))

# [2.17.0](http://bitbucket.org/thermsio/icons/compare/v2.16.0...v2.17.0) (2022-12-19)


### Features

* update icons; ([10964a8](http://bitbucket.org/thermsio/icons/commits/10964a85d883224d3562b26003daacbd2d3f7520))

# [2.16.0](http://bitbucket.org/thermsio/icons/compare/v2.15.0...v2.16.0) (2022-12-19)


### Features

* update pass down icon; ([ea0bce5](http://bitbucket.org/thermsio/icons/commits/ea0bce5ef53b0d023ca7d2b9604973c6df423b03))

# [2.15.0](http://bitbucket.org/thermsio/icons/compare/v2.14.1...v2.15.0) (2022-10-10)


### Features

* update record/property icon and add item mapping for it; ([0be2a80](http://bitbucket.org/thermsio/icons/commits/0be2a80b76f2410a1be3c9c91c35d7d9be8cccd0))

## [2.14.1](http://bitbucket.org/thermsio/icons/compare/v2.14.0...v2.14.1) (2022-08-16)


### Bug Fixes

* node/web constants file ([3397e09](http://bitbucket.org/thermsio/icons/commits/3397e095ceef44314d74b5562b1de83e1c243d15))

# [2.14.0](http://bitbucket.org/thermsio/icons/compare/v2.13.0...v2.14.0) (2022-07-21)


### Features

* add warning about not using Material Symbols; ([bda61d7](http://bitbucket.org/thermsio/icons/commits/bda61d7511345ff6451191299b68e5e98e9ade55))

# [2.13.0](http://bitbucket.org/thermsio/icons/compare/v2.12.0...v2.13.0) (2022-05-26)


### Features

* add users/roster module ([d8cf663](http://bitbucket.org/thermsio/icons/commits/d8cf663bb2008826fbd0cf7fa91a5b78da92bbe6))

# [2.12.0](http://bitbucket.org/thermsio/icons/compare/v2.11.0...v2.12.0) (2022-05-26)


### Bug Fixes

* typo ([12ab2e0](http://bitbucket.org/thermsio/icons/commits/12ab2e043280151ca341ebf0c56047e0c899a968))


### Features

* add missing module icons ([52c0427](http://bitbucket.org/thermsio/icons/commits/52c0427b2f85ce8a50c3e02f8d7cf1d644ab22aa))

# [2.11.0](http://bitbucket.org/thermsio/icons/compare/v2.10.0...v2.11.0) (2022-05-25)


### Features

* update icons; ([d4f861d](http://bitbucket.org/thermsio/icons/commits/d4f861dcb0328d2ea9a8d965e0216ccca3d51f44))

# [2.10.0](http://bitbucket.org/thermsio/icons/compare/v2.9.0...v2.10.0) (2022-05-22)


### Features

* add signal_disconnect icon ([224b7b0](http://bitbucket.org/thermsio/icons/commits/224b7b03bcd709d655cbda49b859879a5e58cf91))

# [2.9.0](http://bitbucket.org/thermsio/icons/compare/v2.8.0...v2.9.0) (2022-05-22)


### Features

* update deps ([2d12b99](http://bitbucket.org/thermsio/icons/commits/2d12b99c6092a452a6dc47ceac092c25831b2bc7))

# [2.8.0](http://bitbucket.org/thermsio/icons/compare/v2.7.0...v2.8.0) (2022-05-19)


### Features

* delete.svg created online with Bitbucket ([d1b50f4](http://bitbucket.org/thermsio/icons/commits/d1b50f460930951301f774418362793fec9a9f9f))

# [2.7.0](http://bitbucket.org/thermsio/icons/compare/v2.6.0...v2.7.0) (2022-05-19)


### Features

* delete.svg created online with Bitbucket ([92aeb1b](http://bitbucket.org/thermsio/icons/commits/92aeb1b6a070664418a411638d19d4dab0c37dd7))

# [2.6.0](http://bitbucket.org/thermsio/icons/compare/v2.5.0...v2.6.0) (2022-03-14)


### Features

* force pipeline re run to update icon links to major version numbers; ([cef30f4](http://bitbucket.org/thermsio/icons/commits/cef30f41d036dd951870d36bb7403c92c535b6e6))

# [2.5.0](http://bitbucket.org/thermsio/icons/compare/v2.4.0...v2.5.0) (2022-03-14)


### Features

* **CORE-1823:** hard code BASE_URL on build time; ([8393a93](http://bitbucket.org/thermsio/icons/commits/8393a931ddac4b21b985b7c54b71428285e64263))

# [2.4.0](http://bitbucket.org/thermsio/icons/compare/v2.3.0...v2.4.0) (2022-03-11)


### Features

* **CORE-1823:** add index to npm package; ([835da4c](http://bitbucket.org/thermsio/icons/commits/835da4cbab0021f8fa1154f42f7dcdd75111094d))

# [2.3.0](http://bitbucket.org/thermsio/icons/compare/v2.2.0...v2.3.0) (2022-03-11)


### Features

* **CORE-1823:** force semantic release; ([ae1d006](http://bitbucket.org/thermsio/icons/commits/ae1d0066c4a173404cffa66f89e4d54463f9aa31))

# [2.2.0](http://bitbucket.org/thermsio/icons/compare/v2.1.0...v2.2.0) (2022-03-11)


### Bug Fixes

* **CORE-1823:** fix test failing; ([d897c22](http://bitbucket.org/thermsio/icons/commits/d897c220e1b047dd215a75719d8faeef2242b53d))


### Features

* **CORE-1823:** inject ICONS_HOST_BASE_URL at build time; ([5c6d131](http://bitbucket.org/thermsio/icons/commits/5c6d13121c0a71f3052a689551b7f4241e2b34f8))

# [2.1.0](http://bitbucket.org/thermsio/icons/compare/v2.0.0...v2.1.0) (2022-03-11)


### Features

* **CORE-1823:** add string to icon and feature name types; ([61a19a0](http://bitbucket.org/thermsio/icons/commits/61a19a017bf5f2ab32a78509263cd5a52946ebdb))

# [2.0.0](http://bitbucket.org/thermsio/icons/compare/v1.17.5...v2.0.0) (2022-03-11)


### Features

* **CORE-1823:** update ts declaration with new exports; ([1aaa77b](http://bitbucket.org/thermsio/icons/commits/1aaa77b01050bce171ba55dff01e7a2fbe3471ec))


### BREAKING CHANGES

* **CORE-1823:** MapFiles names changed, icon names changed; consuming apps must update all of their usages to work with this version.

## [1.17.5](http://bitbucket.org/thermsio/icons/compare/v1.17.4...v1.17.5) (2022-03-11)


### Reverts

* Revert "fix(CORE-1823): force pipeline re run;" ([8f81705](http://bitbucket.org/thermsio/icons/commits/8f81705a9b51e4b13be09d45f63ccba77fd05f49))

## [1.17.4](http://bitbucket.org/thermsio/icons/compare/v1.17.3...v1.17.4) (2022-03-11)


### Bug Fixes

* **CORE-1823:** force pipeline re run; ([cfc8467](http://bitbucket.org/thermsio/icons/commits/cfc84672192d02ebca158c6a762d37c7e286ae40))

## [1.17.3](http://bitbucket.org/thermsio/icons/compare/v1.17.2...v1.17.3) (2022-03-11)


### Bug Fixes

* **CORE-1823:** remove second build step (I'm an idiot); ([5705422](http://bitbucket.org/thermsio/icons/commits/570542235f1e7276fca8622abafaa511c95a41a8))

## [1.17.2](http://bitbucket.org/thermsio/icons/compare/v1.17.1...v1.17.2) (2022-03-11)


### Bug Fixes

* **CORE-1823:** fix pipeline broken because bitbucket does not allow for more than one deployment per pipeline ; ([506bb65](http://bitbucket.org/thermsio/icons/commits/506bb6527cf5e067b6810fe1b48e05f1ce419d86))
* **CORE-1823:** fix pipeline error; ([d532501](http://bitbucket.org/thermsio/icons/commits/d532501db0060f096d93d54832ab62cc35054db5))
* **CORE-1823:** fix using branch specific urls; ([9d3f3f8](http://bitbucket.org/thermsio/icons/commits/9d3f3f866043efe7f75c03fd3104223cd49daeb7))

## [1.17.1](http://bitbucket.org/thermsio/icons/compare/v1.17.0...v1.17.1) (2022-03-10)


### Bug Fixes

* **CORE-1823:** fix build not building; ([07ea1b8](http://bitbucket.org/thermsio/icons/commits/07ea1b8e872a02a0494dbfb14655b9dd27f8979e))

# [1.17.0](http://bitbucket.org/thermsio/icons/compare/v1.16.1...v1.17.0) (2022-03-09)


### Features

* **CORE-1823:** force next minor version; ([61e0ac1](http://bitbucket.org/thermsio/icons/commits/61e0ac1cbd347b0149a42a25efa1e5481800227e))

## [1.16.1](http://bitbucket.org/thermsio/icons/compare/v1.16.0...v1.16.1) (2022-03-09)


### Bug Fixes

* **CORE-1823:** fix build errors; ([68e6d59](http://bitbucket.org/thermsio/icons/commits/68e6d597c9b0da22324f044d51e0d4672702dd49))

# [1.16.0](http://bitbucket.org/thermsio/icons/compare/v1.15.1...v1.16.0) (2022-03-09)


### Bug Fixes

* **CORE-1823:** add next as release; ([a53329a](http://bitbucket.org/thermsio/icons/commits/a53329abf49f36c960ee5824832b1c651b6d07d0))
* **CORE-1823:** clean script breaking pipeline ([a0a70cc](http://bitbucket.org/thermsio/icons/commits/a0a70cc65089537fb8431fe9d5a758e2fe74620a))
* **CORE-1823:** createTSDeclaration SVG dir not existing; ([7cc8977](http://bitbucket.org/thermsio/icons/commits/7cc89771fae6add1c679046c5d1f241a834748e8))
* **CORE-1823:** fix pipeline config; ([11e1401](http://bitbucket.org/thermsio/icons/commits/11e1401af4634b726d19bcbbce828ba627256e22))
* **CORE-1823:** fix pipeline config; ([33b7140](http://bitbucket.org/thermsio/icons/commits/33b7140611e15ad22985f6158baa6f6073172b81))
* **CORE-1823:** fix tests; ([c0f9ffa](http://bitbucket.org/thermsio/icons/commits/c0f9ffad061ed59dc2fd5fdc6ddda16a77f1882d))
* **CORE-1823:** pre-commit not working; ([68c9272](http://bitbucket.org/thermsio/icons/commits/68c9272c9e738a738b62f0c68c9433b1d9983982))
* **CORE-1823:** prettier not running; ([e4551f4](http://bitbucket.org/thermsio/icons/commits/e4551f4b3d6466405e139edf7cb31a42ddab5070))


### Features

* **CORE-1823:** add error catching; ([9abcba8](http://bitbucket.org/thermsio/icons/commits/9abcba877a92e9334ccf7a1605d549d119480ec2))
* **CORE-1823:** add jest; ([e860f54](http://bitbucket.org/thermsio/icons/commits/e860f541a5fffa372b06cd699fb1297401a6f096))
* **CORE-1823:** build icons as a pre-push hook; ([ba4902a](http://bitbucket.org/thermsio/icons/commits/ba4902a3c04cbaa7f3811c7577cdf3ff29320825))
* **CORE-1823:** create map files; ([20ee428](http://bitbucket.org/thermsio/icons/commits/20ee428d21f5bb97e9920b792facc8b0cef98639))
* **CORE-1823:** create TS Declarations; ([cbc0c41](http://bitbucket.org/thermsio/icons/commits/cbc0c4103f2922f4f0c21aa4293dd1e0fde8468d))
* **CORE-1823:** export png files; ([606e0af](http://bitbucket.org/thermsio/icons/commits/606e0afa562243a16610cbaee8afad26cae8967d))
* **CORE-1823:** update index and exported published files; ([4bf23a1](http://bitbucket.org/thermsio/icons/commits/4bf23a1284248f8cf348fc440fea6a58e04eef1d))
* **CORE-1823:** update pipeline variables; ([9a27304](http://bitbucket.org/thermsio/icons/commits/9a27304f65fcdd83dc61dd24d588bc0b25b8ddea))
* **CORE-1823:** update readme; ([9c090ac](http://bitbucket.org/thermsio/icons/commits/9c090acdd099c417d9cc252b470f074a437f90d3))
* **CORE-1823:** update writeIconsShowcase; ([df5b377](http://bitbucket.org/thermsio/icons/commits/df5b3775ed4bff1ad5f7192ff62272efbfcce1f6))
* export svgs; ([93c30bf](http://bitbucket.org/thermsio/icons/commits/93c30bf520576124b77261f2e7bedd5a4967f806))

## [1.15.1](http://bitbucket.org/thermsio/icons/compare/v1.15.0...v1.15.1) (2022-02-03)


### Bug Fixes

* all/done; ([6f20c9b](http://bitbucket.org/thermsio/icons/commits/6f20c9b9a98eab35f3ec1d979e6899348285c786))

# [1.15.0](http://bitbucket.org/thermsio/icons/compare/v1.14.1...v1.15.0) (2022-01-28)


### Features

* done.svg ([b158295](http://bitbucket.org/thermsio/icons/commits/b1582959c4fee86a6af5fd204308cdd766f06e47))
* done.svg ([5982314](http://bitbucket.org/thermsio/icons/commits/5982314466619c07f56f3de583c1017cbf60dfb9))

## [1.14.1](http://bitbucket.org/thermsio/icons/compare/v1.14.0...v1.14.1) (2022-01-28)


### Bug Fixes

* remove done ([333f254](http://bitbucket.org/thermsio/icons/commits/333f254b3a44ca5965da13dcd1e706b96b2780fd))

# [1.14.0](http://bitbucket.org/thermsio/icons/compare/v1.13.1...v1.14.0) (2022-01-28)


### Features

* update all/done; ([ed61a74](http://bitbucket.org/thermsio/icons/commits/ed61a741db3b04479f9cca1ec2678c1356f2639f))

## [1.13.1](http://bitbucket.org/thermsio/icons/compare/v1.13.0...v1.13.1) (2022-01-28)


### Bug Fixes

* done.svg file ext ([ed8cfb0](http://bitbucket.org/thermsio/icons/commits/ed8cfb0b56d8222c7264f7a962e3311c985b86b9))

# [1.13.0](http://bitbucket.org/thermsio/icons/compare/v1.12.0...v1.13.0) (2022-01-28)


### Features

* move theme general icons to all folder. ([be6497f](http://bitbucket.org/thermsio/icons/commits/be6497fa237a1197d8552ea396d04fe1813d0262))

# [1.12.0](http://bitbucket.org/thermsio/icons/compare/v1.11.0...v1.12.0) (2022-01-28)


### Features

* add done ([de40c09](http://bitbucket.org/thermsio/icons/commits/de40c09b98de98e5758b9ed067e11bd1093f9fc9))

# [1.11.0](http://bitbucket.org/thermsio/icons/compare/v1.10.0...v1.11.0) (2022-01-18)


### Features

* **CORE-1668:** add menu icon. ([cce7e40](http://bitbucket.org/thermsio/icons/commits/cce7e40dce50123b6f323eeade0ed31f02e52d4a))

# [1.10.0](http://bitbucket.org/thermsio/icons/compare/v1.9.1...v1.10.0) (2022-01-18)


### Features

* **CORE-1668:** add dark and light theme icons. ([f54ffc3](http://bitbucket.org/thermsio/icons/commits/f54ffc38259f3479348ff8d9abc059efd2d53ffd))

## [1.9.1](http://bitbucket.org/thermsio/icons/compare/v1.9.0...v1.9.1) (2022-01-18)


### Bug Fixes

* **CORE-1775:** update getFormattedName typings and return shape ([35bd398](http://bitbucket.org/thermsio/icons/commits/35bd39838a3702c4ea07c6c68911d34a9c0f6030))

# [1.9.0](http://bitbucket.org/thermsio/icons/compare/v1.8.7...v1.9.0) (2022-01-17)


### Features

* **CORE-1775:** fix module exports; ([c1053a7](http://bitbucket.org/thermsio/icons/commits/c1053a7e81fdde9c3a1b32f11e39352d4c9da10a))
* **CORE-1775:** getFormattedNames, return consistent shape; ([9cf2bf9](http://bitbucket.org/thermsio/icons/commits/9cf2bf9c9f23db93aaaf66da4df9f92cc6560858))

## [1.8.7](http://bitbucket.org/thermsio/icons/compare/v1.8.6...v1.8.7) (2022-01-17)


### Bug Fixes

* **CORE-1775:** fix typo; ([ae0823d](http://bitbucket.org/thermsio/icons/commits/ae0823d1f9eef895065164438b4fb83f51100b4c))

## [1.8.6](http://bitbucket.org/thermsio/icons/compare/v1.8.5...v1.8.6) (2022-01-17)


### Bug Fixes

* **CORE-1775:** fix TS declaration; ([24719f8](http://bitbucket.org/thermsio/icons/commits/24719f8e54f963ac8adf9c71d1be31bc97bb5443))

## [1.8.5](http://bitbucket.org/thermsio/icons/compare/v1.8.4...v1.8.5) (2022-01-17)


### Bug Fixes

* **CORE-1775:** fix TS declaration; ([3238c41](http://bitbucket.org/thermsio/icons/commits/3238c41d1c04fe69d30bb2fc1796d4af8d5102ce))

## [1.8.4](http://bitbucket.org/thermsio/icons/compare/v1.8.3...v1.8.4) (2022-01-17)


### Bug Fixes

* **CORE-1775:** correctly export TS types and module; ([e8801d9](http://bitbucket.org/thermsio/icons/commits/e8801d942768094c02fca786d6ca2ddd11ff0144))

## [1.8.3](http://bitbucket.org/thermsio/icons/compare/v1.8.2...v1.8.3) (2022-01-17)


### Bug Fixes

* **CORE-1775:** export utils; ([ead0dd6](http://bitbucket.org/thermsio/icons/commits/ead0dd6aec0103d3eb7d41af896b484a06f64749))

## [1.8.2](http://bitbucket.org/thermsio/icons/compare/v1.8.1...v1.8.2) (2022-01-17)


### Bug Fixes

* **CORE-1775:** fix utils export; ([83b032c](http://bitbucket.org/thermsio/icons/commits/83b032ce52f65cb6b6f5c609b2ddf363d5245b7c))

## [1.8.1](http://bitbucket.org/thermsio/icons/compare/v1.8.0...v1.8.1) (2022-01-17)


### Bug Fixes

* **CORE-1775:** bundle types.ts; ([da0a68f](http://bitbucket.org/thermsio/icons/commits/da0a68f2932e18d135eeab874a484728a1643fc0))

# [1.8.0](http://bitbucket.org/thermsio/icons/compare/v1.7.0...v1.8.0) (2022-01-14)


### Bug Fixes

* **CORE-1775:** update src map file with kebab-case; ([261a093](http://bitbucket.org/thermsio/icons/commits/261a093fd33e28f32b7218ad163edebf16caa984))


### Features

* **CORE-1775:** add getFormattedName; ([f15debf](http://bitbucket.org/thermsio/icons/commits/f15debf20209fc6edbd28daaed0471282d5957f3))
* **CORE-1775:** export from index file; ([035b025](http://bitbucket.org/thermsio/icons/commits/035b025a4f350001173cd2d1a5701255c8fecaa6))
* **CORE-1775:** FeatureNames, IconNames types, allow string ([dc84e97](http://bitbucket.org/thermsio/icons/commits/dc84e971c0d8540370d3cfc7b0a3da91040a6689))
* **CORE-1775:** reverse back to kebab-case icon files ([e8ce844](http://bitbucket.org/thermsio/icons/commits/e8ce844303a599afd4e504734f2243d7bead752f))

# [1.7.0](http://bitbucket.org/thermsio/icons/compare/v1.6.0...v1.7.0) (2022-01-14)


### Features

* **CORE-1775:** update REPO-INFO; The real reason behind this commit is to publish a new version to npm ([8e63e77](http://bitbucket.org/thermsio/icons/commits/8e63e77848012f286d8dc7e92a88f4145f34a97c))

# [1.6.0](http://bitbucket.org/thermsio/icons/compare/v1.5.0...v1.6.0) (2022-01-14)


### Features

* **CORE-1775:** create map types WIP ([45f00c3](http://bitbucket.org/thermsio/icons/commits/45f00c3afdfe4120d87fe588c1c76ad849d56922))
* **CORE-1775:** export IconNames and FeatureNames types ([3aa26ca](http://bitbucket.org/thermsio/icons/commits/3aa26ca66c2718f63a9e1700d5e07437493ec0e3))
* **CORE-1775:** rename icons to PascalCase ([35fb23e](http://bitbucket.org/thermsio/icons/commits/35fb23e31418d165d26172fba00e245cc6a5c951))

# [1.5.0](http://bitbucket.org/thermsio/icons/compare/v1.4.0...v1.5.0) (2022-01-13)


### Features

* **CORE-1775:** move lib files to root of project ([9f528e2](http://bitbucket.org/thermsio/icons/commits/9f528e2e9b8657ec142b76e27b8918882840e0ed))

# [1.4.0](http://bitbucket.org/thermsio/icons/compare/v1.3.0...v1.4.0) (2022-01-13)


### Features

* **CORE-1775:** fix HOST_BASE_URL on pipeline file ([1529518](http://bitbucket.org/thermsio/icons/commits/1529518cacb66a15bd02a8b635ba4bc762171f45))

# [1.3.0](http://bitbucket.org/thermsio/icons/compare/v1.2.0...v1.3.0) (2022-01-13)


### Features

* **CORE-1775:** remove outdated comment; force deploy to update map.json; ([5814829](http://bitbucket.org/thermsio/icons/commits/5814829129cb8106d1b07905ec24d44710ae80e3))

# [1.2.0](http://bitbucket.org/thermsio/icons/compare/v1.1.0...v1.2.0) (2022-01-13)


### Bug Fixes

* **CORE-1775:** pngs files being converted with sub structure. ([29baa9d](http://bitbucket.org/thermsio/icons/commits/29baa9d23f2e3e9792889060d0465e9201b80fdb))


### Features

* **CORE-1775:** add map-web-svg as main entry point. ([77d2898](http://bitbucket.org/thermsio/icons/commits/77d2898937c45d9b2b72596b9484b47bc9dd55f5))
* **CORE-1775:** add new reference map.json file. ([463b3cf](http://bitbucket.org/thermsio/icons/commits/463b3cf9b1107ac61a333b75113f6b3b8b3f244e))
* **CORE-1775:** add referenced icons. ([79589e0](http://bitbucket.org/thermsio/icons/commits/79589e0f35486f907e8a2d213bc4724f30233cad))
* **CORE-1775:** add test to make sure there are no feature names used as icon names. ([75fcdb7](http://bitbucket.org/thermsio/icons/commits/75fcdb77cac388e8ea76a247472068a96a7f287f))
* **CORE-1775:** change test to check for square view boxes instead of 192px view boxes. ([df7f788](http://bitbucket.org/thermsio/icons/commits/df7f788e9525f863ba69dc39b629ac5af8497f51))
* **CORE-1775:** copy feature specific icons to root if they do not exist. ([428bb1f](http://bitbucket.org/thermsio/icons/commits/428bb1fe00959381a838ef7fd0b81a402269be52))
* **CORE-1775:** create map file for every platform and extension. ([0474f21](http://bitbucket.org/thermsio/icons/commits/0474f2132d50471a4cb8862ae5c369430406e786))
* **CORE-1775:** update icons for the new logic and to be usage based. ([beb6f99](http://bitbucket.org/thermsio/icons/commits/beb6f99ab153f52ce63e6be8760f755d32a6ce8b))
* **CORE-1775:** update readme. ([46565c1](http://bitbucket.org/thermsio/icons/commits/46565c1c34230ab479a1871f4cb6562c7f3d5d50))

# [1.1.0](http://bitbucket.org/thermsio/icons/compare/v1.0.1...v1.1.0) (2021-12-30)


### Bug Fixes

* **CORE-1446:** hash link on readme ([b514660](http://bitbucket.org/thermsio/icons/commits/b514660d923b828671d377e1f6e9f8bd7d9ef0ba))
* **CORE-1446:** test now evaluates source folder ([062c266](http://bitbucket.org/thermsio/icons/commits/062c266fb1a88f174e3205616d461e21344414f4))
* package.json repo url ([5299c85](http://bitbucket.org/thermsio/icons/commits/5299c8564fca6c971c6dd546a1423ef7439baa73))


### Features

* add package entry; ([0db886b](http://bitbucket.org/thermsio/icons/commits/0db886bd11c62d714cf31262e0e3942e52649376))
* **CORE-1446:** add android icons ([c1e763a](http://bitbucket.org/thermsio/icons/commits/c1e763a656f46da04c9c1755e39e9d598770db0e))
* **CORE-1446:** add build script ([0d11404](http://bitbucket.org/thermsio/icons/commits/0d11404dd90674845c174c894fc669dd0f131ed8))
* **CORE-1446:** add icons to repo as part of the build process ([e05a0fe](http://bitbucket.org/thermsio/icons/commits/e05a0fef9b9cb3215b5899a5ad6bfcb87d2c3694))
* **CORE-1446:** add map file builder script ([be6a302](http://bitbucket.org/thermsio/icons/commits/be6a3020008339a403909c566407e1f6d27fe329))
* **CORE-1446:** add png files support ([134fdf8](http://bitbucket.org/thermsio/icons/commits/134fdf84982e732b28b9fea81c683aa6e2cc46f0))
* **CORE-1446:** build icons from src rather than figma ([ad41c17](http://bitbucket.org/thermsio/icons/commits/ad41c173d7710c1abd977e4e431f2038f7fb5ec9))
* **CORE-1446:** format map.json ([71ca4bc](http://bitbucket.org/thermsio/icons/commits/71ca4bcccef8d8bd2950fe85b4733051e134dd56))
* **CORE-1446:** update README.md ([214042f](http://bitbucket.org/thermsio/icons/commits/214042f8fdac91fc3cce3a91188f6b616e162c59))

import path from 'path'
import fs from 'fs/promises'
import { optimize } from 'svgo'
import svgoConfig from '../../svgo.config.cjs'

const getSvgsFromDir = async (dir) => {
  try {
    const svgNames = await fs.readdir(dir)

    const svgs = await Promise.all(
      svgNames.map(async (name) => {
        const svg = await fs.readFile(path.resolve(dir, name))
        const optimizedSvg = optimize(svg, svgoConfig).data

        return {
          name,
          content: optimizedSvg,
        }
      }),
    )

    return svgs
  } catch (error) {
    console.log(error)
    process.exit(1)
  }

  return []
}

export default getSvgsFromDir

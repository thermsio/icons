/* eslint-disable no-param-reassign */
const fs = require('fs')
const path = require('path')
const urlJoin = require('url-join')
const { EXTENSIONS, platforms } = require('../constants-node.js')
const { BASE_URL } = require('../constants-web.js')

const getReadableIconFilesData = () => {
  /**
   * Build the icons object data for each platform
   */
  const getIconsByName = (iconType) => {
    const _iconsByName = {}

    fs.readdirSync(path.resolve(__dirname, '..', 'icons', iconType)).forEach(
      (filename) => {
        const name = filename.substring(0, filename.indexOf('.'))
        const filenameWithoutExt = filename.replace(/(\.svg)$/gi, '')

        if (!_iconsByName[name]) {
          _iconsByName[name] = {
            android: undefined,
            ios: undefined,
            web: undefined,
          }
        }

        if (filename.includes('.ios')) {
          _iconsByName[name].ios = filenameWithoutExt
        } else if (filename.includes('.android')) {
          _iconsByName[name].android = filenameWithoutExt
        } else {
          _iconsByName[name].web = filenameWithoutExt
        }
      },
    )

    /**
     *  Fill in the default filename for all platforms when there is not a specific filename previously assigned
     */
    Object.values(_iconsByName).forEach((icon) => {
      let defaultFilename

      if (icon.web) {
        defaultFilename = icon.web
      }
      if (icon.ios && !defaultFilename) {
        defaultFilename = icon.ios
      }
      if (icon.android && !defaultFilename) {
        defaultFilename = icon.android
      }

      if (!icon.web) icon.web = defaultFilename
      if (!icon.ios) icon.ios = defaultFilename
      if (!icon.android) icon.android = defaultFilename
    })

    return _iconsByName
  }

  /**
   *  Build platform specific details for each icon file
   */
  const getIconUrls = (_iconsByName) => {
    const _iconUrls = {
      png: {
        android: {},
        ios: {},
        web: {},
      },
      svg: {
        android: {},
        ios: {},
        web: {},
      },
    }

    Object.entries(_iconsByName).forEach(([name, icon]) => {
      EXTENSIONS.forEach((ext) => {
        platforms.forEach((platform) => {
          const uri = urlJoin(BASE_URL, ext, `${icon[platform]}.${ext}`)

          _iconUrls[ext][platform][name] = uri.toString()

          if (typeof _iconUrls[ext][platform][name] !== 'string') {
            console.log('')
            console.log('')
            console.log('ERROR', name)
            console.log('')
            console.log('')
          }
        })
      })
    })

    return _iconUrls
  }

  /**
   [icon name]: {
   [platform]: [filepath without extension]
 }
   */
  const allIconsByName = getIconsByName('all')
  const referencedIconsByName = getIconsByName('referenced')

  /**
   [extension]: {
   [platform]: {
     [icon name]: [url for icon]
   }
 }
   */
  const allIconUrls = getIconUrls(allIconsByName)
  const referencedIconUrls = getIconUrls(referencedIconsByName)

  return {
    allIconsByName,
    allIconUrls,
    referencedIconsByName,
    referencedIconUrls,
  }
}

module.exports.getReadableIconFilesData = getReadableIconFilesData

import urlJoin from 'url-join'
import { BASE_URL } from '../constants-web.js'

const getUrlFromIconPath = (path, extension) => {
  console.log({ BASE_URL, path, extension })
  return urlJoin(BASE_URL, extension, `${path}.${extension}`)
}

export default getUrlFromIconPath

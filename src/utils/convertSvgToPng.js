import sharp from 'sharp'

const convertSvgToPng = (path) => sharp(path).resize(72).png().toBuffer()

export default convertSvgToPng

import fs from 'fs/promises'
import { MAP_FEATURES_PATH } from '../constants-node.js'

const getFeatureNames = async () => {
  try {
    const featuresMap = await fs
      .readFile(MAP_FEATURES_PATH)
      .then((data) => JSON.parse(data))
    return Object.keys(featuresMap)
  } catch (error) {
    console.log(error)
    process.exit(1)
    return undefined
  }
}

export default getFeatureNames

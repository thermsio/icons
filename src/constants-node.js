import 'dotenv/config'
import path from 'path'

const __dirname = new URL('.', import.meta.url).pathname

export const BASE_DECLARATION_PATH = path.resolve(
  __dirname,
  'baseDeclaration.d.ts',
)
export const EXTENSIONS = ['png', 'svg']
export const INDEX_PATH = path.resolve(__dirname, '..', 'index.d.ts')
export const MAP_FEATURES_PATH = path.resolve(__dirname, 'map.features.json')
export const MATERIAL_DIR = path.resolve(
  __dirname,
  '..',
  'node_modules',
  '@material-design-icons',
  'svg',
  'round',
)
export const PNG_DIR = path.resolve(__dirname, '..', 'png')
export const README_PATH = path.resolve(__dirname, '..', 'README.md')
export const ROOT_DIR = path.resolve(__dirname, '..')
export const SRC_DIR = path.resolve(__dirname, 'icons')
export const SVG_DIR = path.resolve(__dirname, '..', 'svg')

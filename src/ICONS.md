- All svg icons that exist in `src/icons/all` directory will be included in the platform map `.json` files.


- Any .svg files that exist in the `/referenced` dir will only be included when they are referenced in
  the `src/map.json`. The `src/map.json` file is used to map module specific semantics to an icon name. Example:

 ```
    {
        [module name]: {
            [semantic name]: [file name in src/icons/all|referenced]
        }
    }
 ```
import chalk from 'chalk'
import fs from 'fs'
import createSVG from './createSVG/index.js'
import { PNG_DIR, SVG_DIR } from '../constants-node.js'
import createPNG from './createPNG/index.js'

if (!fs.existsSync(SVG_DIR)) fs.mkdirSync(SVG_DIR)

console.log(chalk.yellow.bold('- Creating SVG Files'))
await createSVG().then(() =>
  console.log(chalk.bgYellow.black.bold('SVG Files Created')),
)

if (!fs.existsSync(PNG_DIR)) fs.mkdirSync(PNG_DIR)

console.log(chalk.magenta.bold('- Creating PNG Files from SVG'))
await createPNG().then(() =>
  console.log(chalk.bgMagenta.black.bold('PNG Files Created')),
)

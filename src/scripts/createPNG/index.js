import chalk from 'chalk'
import fs from 'fs/promises'
import glob from 'glob'
import path from 'path'
import convertSvgToPng from '../../utils/convertSvgToPng.js'
import { PNG_DIR, SVG_DIR } from '../../constants-node.js'

const createPNG = () => {
  const promises = []

  try {
    glob(path.resolve(SVG_DIR, '*.svg'), null, (err, files) => {
      if (err) {
        console.log(
          chalk.red(`There was an error fetching SVG Files to convert as PNG.`),
        )
        console.log(err)
        process.exit(1)
      }

      files.forEach(async (file) => {
        const filePath = path.parse(file)
        const pngBuffer = await convertSvgToPng(file)

        promises.push(
          fs.writeFile(
            path.format({ dir: PNG_DIR, name: filePath.name, ext: '.png' }),
            pngBuffer,
          ),
        )
      })
    })
  } catch (error) {
    console.log(chalk.red(`There was an error writing converted PNG files.`))
    console.log(error)

    process.exit(1)
    return Promise.reject()
  }

  return Promise.all(promises)
}

export default createPNG

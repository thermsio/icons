const createTSNameOptions = (names) => {
  let nameOption = ''
  names.forEach((name, index) => {
    nameOption +=
      index === names.length - 1 ? `'${name}' | string` : `'${name}' | `
  })

  return nameOption
}

export default createTSNameOptions

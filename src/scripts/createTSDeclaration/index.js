import chalk from 'chalk'
import fs from 'fs/promises'
import path from 'path'
import prettier from 'prettier'
import createTSNameOptions from './createTSNameOptions.js'
import getFeatureNames from '../../utils/getFeatureNames.js'
import { BASE_DECLARATION_PATH, INDEX_PATH, SVG_DIR } from '../../constants-node.js'

const createTSDeclaration = async () => {
  let declarationFile
  try {
    declarationFile = await fs.readFile(BASE_DECLARATION_PATH)
    const iconNames = await fs
      .readdir(SVG_DIR)
      .then((icons) => icons.map((icon) => icon.split('.')[0]))
    const featureNames = await getFeatureNames()

    declarationFile += `export type IconNames = ${createTSNameOptions(
      iconNames,
    )}\n`

    declarationFile += `export type FeatureNames = ${createTSNameOptions(
      featureNames,
    )}\n`
  } catch (error) {
    console.log(chalk.red('There was an error writing TS Declaration file.'))
    console.log(error)
    process.exit(1)
    return Promise.reject()
  }

  const formattedDeclaration = prettier.format(declarationFile, {
    parser: 'babel-ts',
    semi: false,
  })

  return fs.writeFile(path.resolve(INDEX_PATH), formattedDeclaration)
}

export default createTSDeclaration

import jsonfile from 'jsonfile'
import getUrlFromIconPath from '../../utils/getUrlFromIconPath.js'
import { EXTENSIONS, MAP_FEATURES_PATH } from '../../constants-node.js'

const featuresMap = await jsonfile.readFile(MAP_FEATURES_PATH)

const getFeaturesUrlMap = () => {
  const featuresUrlMap = {}

  EXTENSIONS.forEach((extension) => {
    featuresUrlMap[extension] = {}

    Object.entries(featuresMap).forEach(([feature, icons]) => {
      featuresUrlMap[extension][feature] = {}

      Object.entries(icons).forEach(([name, iconPath]) => {
        featuresUrlMap[extension][feature][name] = getUrlFromIconPath(
          iconPath,
          extension,
        )
      })
    })
  })

  return featuresUrlMap
}

export default getFeaturesUrlMap

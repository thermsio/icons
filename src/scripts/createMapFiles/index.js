import chalk from 'chalk'
import fs from 'fs/promises'
import path from 'path'
import prettier from 'prettier'
import getUIUrlMap from './getUIUrlMap.js'
import getFeaturesUrlMap from './getFeaturesUrlMap.js'
import { EXTENSIONS, ROOT_DIR } from '../../constants-node.js'

const featuresUrlMap = await getFeaturesUrlMap()
const UIUrlMap = await getUIUrlMap()

const createMapFiles = () => {
  const promises = []

  try {
    EXTENSIONS.forEach((extension) => {
      const capitalizedExtension =
        extension[0].toUpperCase() + extension.slice(1)

      const formattedFeatures = prettier.format(
        JSON.stringify(featuresUrlMap[extension]),
        { parser: 'json' },
      )
      promises.push(
        fs.writeFile(
          path.resolve(ROOT_DIR, `MapFeatures${capitalizedExtension}.json`),
          formattedFeatures,
        ),
      )

      const formattedUI = prettier.format(JSON.stringify(UIUrlMap[extension]), {
        parser: 'json',
      })
      promises.push(
        fs.writeFile(
          path.resolve(ROOT_DIR, `MapUI${capitalizedExtension}.json`),
          formattedUI,
        ),
      )
    })
  } catch (error) {
    console.log(chalk.red(`There was an error creating Map files.`))
    console.log(error)

    process.exit(1)
    return Promise.reject()
  }

  return Promise.all(promises)
}

export default createMapFiles

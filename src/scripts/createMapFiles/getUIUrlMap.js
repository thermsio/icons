import fs from 'fs/promises'
import getUrlFromIconPath from '../../utils/getUrlFromIconPath.js'
import { EXTENSIONS, MATERIAL_DIR } from '../../constants-node.js'

const getUIUrlMap = async () => {
  const UIUrlMap = {}
  const UIIcons = await fs.readdir(MATERIAL_DIR)

  EXTENSIONS.forEach((extension) => {
    UIUrlMap[extension] = {}

    UIIcons.forEach((icon) => {
      const name = icon.split('.')[0]
      UIUrlMap[extension][name] = getUrlFromIconPath(name, extension)
    })
  })

  return UIUrlMap
}

export default getUIUrlMap

import chalk from 'chalk'
import fs from 'fs/promises'
import path from 'path'
import getAllIcons from './getAllIcons.js'
import { SVG_DIR } from '../../constants-node.js'

const createSVG = async () => {
  try {
    const icons = await getAllIcons()

    return Promise.all(
      icons.map(({ name, content }) =>
        fs.writeFile(path.resolve(SVG_DIR, name), content),
      ),
    )
  } catch (error) {
    console.log(chalk.red(`There was an error writing SVG files.`))
    console.log(error)

    process.exit(1)
    return Promise.reject()
  }
}

export default createSVG

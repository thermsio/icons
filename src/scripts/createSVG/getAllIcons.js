import getSvgsFromDir from '../../utils/getSvgsFromDir.js'
import { MATERIAL_DIR, SRC_DIR } from '../../constants-node.js'

const getAllIcons = () =>
  Promise.all([getSvgsFromDir(MATERIAL_DIR), getSvgsFromDir(SRC_DIR)]).then(
    (_icons) => _icons.flat(),
  )

export default getAllIcons

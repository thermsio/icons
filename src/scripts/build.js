import chalk from 'chalk'
import createMapFiles from './createMapFiles/index.js'
import createTSDeclaration from './createTSDeclaration/index.js'
import writeIconsShowcase from './writeIconsShowcase/index.js'

console.log(chalk.blue.bold('- Creating Map Files'))
createMapFiles().then(() =>
  console.log(chalk.bgBlue.black.bold('Map Files Created')),
)

console.log(chalk.green.bold('- Creating Icon Showcase Readme'))
writeIconsShowcase().then(() =>
  console.log(chalk.bgGreen.black.bold('Icons Showcase Written')),
)

console.log(chalk.cyan.bold('- Creating TS Declaration'))
createTSDeclaration().then(() =>
  console.log(chalk.bgCyan.black.bold('TS Declaration Created')),
)

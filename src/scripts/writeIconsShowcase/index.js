import chalk from 'chalk'
import fs from 'fs/promises'
import { getIconUrl } from '../../../utils/index.js'
import { MAP_FEATURES_PATH, README_PATH } from '../../constants-node.js'

/**
 * Builds the README.md file.
 */
const writeIconsShowcase = async () => {
  const featuresMap = await fs
    .readFile(MAP_FEATURES_PATH)
    .then((data) => JSON.parse(data))
  let README = `# Module Specific References\n`

  try {
    Object.entries(featuresMap).forEach(([feature, icons]) => {
      README += `## ${feature}\n\n`

      Object.entries(icons).forEach(([iconName, svgName]) => {
        const iconUrl = getIconUrl(svgName, 'png')

        README += `${iconName}\n\n`
        README += `![${iconName}](${iconUrl})\n\n`
      })
    })
  } catch (error) {
    console.log(chalk.red(`There was an error writing the README.md file.`))
    console.log(error)

    process.exit(1)
    return Promise.reject()
  }

  return fs.writeFile(README_PATH, README, null)
}

export default writeIconsShowcase

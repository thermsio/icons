/* eslint-disable no-unused-vars */
type getFormattedNameType = (
  // eslint-disable-next-line no-unused-vars
  iconName: string,
  featureName?: string,
) => {
  icon?: string
  feature?: string
}

type getIconUrlType = (name: string, extension?: 'svg' | 'png') => string | null

declare const getFormattedName: getFormattedNameType
declare const getIconUrl: getIconUrlType

# THERMS Icons #

This repository contains all the logic for publishing those icons to later be consumed.

README.md hosts all THERMS specific icon names. This should be used as a reference to pick icons that have a specific
meaning for our business. In addition, this repository also contains Google's Material icons, this extensive library
provides a healthy set tools to build UIs.

The different Map.json files contain the url of each icon and can be used in your applications to fetch them.

## Usage

### Web

```
npm install @therms/icons
```

By importing `@therms/icons` you will get the `MapFeaturesSvg.json`, if you wish to work with png you need to
import `@therms/icons/MapFeaturesPng.json`.

We also provide `MapUI*.json` files that contain all the urls for Material Icons. It is a very big file, instead we
recommend searching the desired icon on [Google's Site](https://fonts.google.com/icons), once you know the name of your
icon, use the exported helper function `getIconUrl` to get the url where you should fetch it.

**Note: on Google's site there actually two set of icons, Material Symbols and Material Icons, make sure you are
browsing under the Material Icons category because some Material Symbols icons are not available are on Material
Icons.**

If you are using React we recommend using [svgr](https://react-svgr.com/) to end up with inlined svgs that can be styled
via CSS.

If you don't want to install the npm package you may use the `map.json` file to look up the icon's url to be used on an
image tag.

### Mobile

You can use `MapFeaturesPng.json` or `MapFeaturesSvg.json` to get the urls for the icons.

## Development

When a branch is merged to master, icons will be automatically exported and published to npm.

In order to specify a new icon you need to add it to `src/map.features.json` the key should be the name you wish for the
icon to have and the value should be the name of the icon file (without extension). You are free to use any
of [Google's Icons Names](https://fonts.google.com/icons), The build process will automatically link them to the
corresponding file. If you want to use a custom icon, you need to add it to `src/icons` folder. These icons take
precedence over Google's in case of name clashing.

_We use `snake_case` for icon file naming._

- You should not use an icon name that also the name of an icon group.
- You should only do one level deep of grouping. Subgroups are not supported.

Because we optimize the source files there are some requirements that must be followed by all raw icons.

- They must be on .svg format
- They must be bound in a square view-box (last two numbers should be the same).
- They must not use the `stroke` property to draw their lines.

### Working locally

Some scripts rely on environment variables to work correctly. The easiest way to add them is to create a `.env.local`
file in the root of this repository with the following:

```dotenv
# Base url from where all our icons are made available to consuming clients.
ICONS_HOST_BASE_URL="https://bitbucket.org/thermsio/icons/raw/develop/"
```
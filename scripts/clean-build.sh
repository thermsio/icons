#!/bin/bash

# exit script if any commands fail
set -e

rm -f index.d.ts index.js index.js.map Map*.json README.md
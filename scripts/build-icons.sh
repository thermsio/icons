#!/bin/bash

# exit script if any commands fail
set -e

node src/scripts/buildIcons.js

printf "\033[0;36m \n=== Creating Icons === \n \033[0m"
git add png/* svg/*; git commit -m 'add icons to git'

exit 0

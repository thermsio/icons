#!/bin/bash

# exit script if any commands fail
set -e

node src/scripts/build.js

npx rollup -c